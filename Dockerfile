FROM node:10 AS build-frontend

WORKDIR /frontend
COPY client .
WORKDIR /frontend/whoru
RUN yarn install
RUN yarn build

FROM golang:alpine AS builder

# Install git.
# Git is required for fetching the dependencies.
RUN apk update && apk add --no-cache git

WORKDIR $GOPATH/src/

COPY . .

WORKDIR $GOPATH/src/whoru/

## Fetch dependencies.
RUN go get -d -v 

# Build
RUN go build .

RUN mkdir -p /app/static
RUN cp whoru /app/whoru

COPY --from=build-frontend /frontend/whoru/dist  /app/static

# This container exposes port 8001 to the outside world
EXPOSE 8001

# Run the executable
ENTRYPOINT ["/app/whoru"]
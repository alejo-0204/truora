package db

import (
	"database/sql"
	"fmt"
	"log"
	"strconv"
	"time"
)

import (
	"whoru/models"
	"whoru/querys"
)

// DbInstance : database instance
var dbInstance *sql.DB

func getDBInstance() *sql.DB {
	// return a single db instance
	if dbInstance != nil {
		return dbInstance
	}

	return InitDB()
}

// InitDB : initialize dababase
func InitDB() *sql.DB {
	// Connect to the "truora" database.
	connStr := "host=roach1 port=26257 user=maxroach dbname=truora sslmode=disable"
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal("error connecting to the database: ", err)
	}

	// Create the "server" table.
	if _, err := db.Exec(querys.CreateServerTable); err != nil {
		log.Fatal(err)
	}

	// Create the "adress" table.
	if _, err := db.Exec(querys.CreateAdressTable); err != nil {
		log.Fatal(err)
	}

	dbInstance = db
	return dbInstance
}

// GetCheckHistory : return ordered check history
func GetCheckHistory(limit string) models.History {
	rows, err := getDBInstance().Query(querys.GetAllOrderedServer(limit))
	if err != nil {
		log.Fatal(err)
	}
	// When finish method will close the rows
	defer rows.Close()
	var history models.History
	var serversData models.ExtendedServers
	for rows.Next() {
		var id int64
		var serversChanged, isDown bool
		var domain, sslGrade, previousSslGrade, logo, title string

		// assign the request response for each field in the variables
		if err := rows.Scan(&id, &domain, &serversChanged, &sslGrade, &previousSslGrade, &logo, &title, &isDown); err != nil {
			log.Fatal(err)
		}

		t, _ := time.Parse(models.DateFormat, strconv.FormatInt(id, 10))
		serversData.Domain = domain
		serversData.Date = t.Format("2006-01-02 15:04:05")
		serversData.ServersChanged = serversChanged
		serversData.SslGrade = sslGrade
		serversData.PreviousSslGrade = previousSslGrade
		serversData.Logo = logo
		serversData.Title = title
		serversData.IsDown = isDown
		serversData.Servers.Servers = getAdressesByParentID(id)

		history.Items = append(history.Items, serversData)
	}
	return history
}

// RegisterServerInfo : register the server information
func RegisterServerInfo(domain string, response models.Servers) {
	currentTime := time.Now()
	parentID := currentTime.Format(models.DateFormat)

	insServer := querys.InsServer(parentID,
		domain,
		response.ServersChanged,
		response.SslGrade,
		response.PreviousSslGrade,
		response.Logo,
		response.Title,
		response.IsDown)

	_, err := getDBInstance().Exec(insServer)
	if err != nil {
		log.Fatal(err)
	}

	for _, adress := range response.Servers {
		RegisterAdressInfo(adress, parentID)
	}
}

// RegisterAdressInfo : register the adress information
func RegisterAdressInfo(adress models.Server, parentID string) {
	insAdress := querys.InsAdress(adress.Adress,
		adress.SslGrade,
		adress.Country,
		adress.Owner,
		parentID)
	_, err := getDBInstance().Exec(insAdress)
	if err != nil {
		log.Fatal(err)
	}
}

// GetLatestServersReport : return domain report with the information of one hour ago if exist
func GetLatestServersReport(domain string) models.Servers {
	reportTime := time.Now().Add(-1 * time.Hour)
	dateFormated := reportTime.Format(models.DateFormat)
	rows, err := getDBInstance().Query(querys.GetLastHourReport(dateFormated, domain))
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	var id int64
	var serversChanged, isDown bool
	var storedDomain, sslGrade, previousSslGrade, logo, title string
	var serversData models.Servers
	for rows.Next() {

		if err := rows.Scan(&id, &storedDomain, &serversChanged, &sslGrade, &previousSslGrade, &logo, &title, &isDown); err != nil {
			log.Fatal(err)
		}
		fmt.Printf("%d\n", id)
	}

	serversData.ServersChanged = serversChanged
	serversData.SslGrade = sslGrade
	serversData.PreviousSslGrade = previousSslGrade
	serversData.Logo = logo
	serversData.Title = title
	serversData.IsDown = isDown
	serversData.Servers = getAdressesByParentID(id)

	return serversData
}

func getAdressesByParentID(id int64) []models.Server {
	var adresses []models.Server
	rows, err := getDBInstance().Query(querys.GetAdressByParentID(id))
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	var parentID int64
	var adress, sslGrade, country, owner string
	for rows.Next() {

		if err := rows.Scan(&adress, &sslGrade, &country, &owner, &parentID); err != nil {
			log.Fatal(err)
		}

		adresses = append(adresses, models.Server{
			Adress:   adress,
			SslGrade: sslGrade,
			Country:  country,
			Owner:    owner,
		})
	}

	return adresses
}

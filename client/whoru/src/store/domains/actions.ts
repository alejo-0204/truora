import { IFullState } from "@/domain/models";
import { getDomainInfo, getHistory } from "@/services/checkServices";
import { ActionTree } from "vuex";
import { IRootState } from "../../domain/models";

export const actions: ActionTree<IFullState, IRootState> = {
  fetchHistory({ commit }): any {
    getHistory().then(
      response => {
        const payload: any = response.items;
        commit("historyLoaded", payload);
      },
      error => {
        console.info(error);
        commit("historyError");
      }
    );
  },
  fetchDomain({ commit }, domain): any {
    getDomainInfo(domain).then(
      response => {
        const payload: any = response;
        if (payload) {
          payload.Domain = domain;
        }
        commit("searchResult", payload);
      },
      error => {
        console.info(error);
        commit("error");
      }
    );
  },
  loading({ commit }) {
    commit("loading");
  }
};

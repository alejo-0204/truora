export interface IServer {
  adress: string;
  ssl_grade: string;
  country: string;
  owner: string;
  domain?: string;
  date?: string;
}

export interface IDomain {
  Servers: IServer[] | null | undefined;
  servers_changed: boolean;
  ssl_grade: string;
  previous_ssl_grade: string;
  logo: string;
  title: string;
  is_down: boolean;
  Domain?: string;
  Date?: string;
}

export interface IFullState {
  history: IDomain[];
  error?: {
    message: string;
  };
  searchResult?: IDomain;
  loading: boolean;
}

export interface IRootState {
  version: string;
}

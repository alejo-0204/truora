package main

import (
	"fmt"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"

	"whoru/db"
	"whoru/services"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	_ "github.com/lib/pq"
)

var router *chi.Mux

func routers() *chi.Mux {
	// Routes configuration
	router.Get("/api/v1/check", services.Check)
	router.Get("/api/v1/history", services.History)
	// Serve static files
	FileServer(router, "/", "/app/static")
	return router
}

// FileServer is serving static files
func FileServer(r chi.Router, public string, static string) {

	// check if public url contains any of the {}* characters
	if strings.ContainsAny(public, "{}*") {
		panic("FileServer does not permit URL parameters.")
	}

	// change the static url to the compatible OS
	root, _ := filepath.Abs(static)

	// get info of directory and check error if directory not exists
	if _, err := os.Stat(root); os.IsNotExist(err) {
		panic("Static Documents Directory Not Found")
	}

	//
	fs := http.StripPrefix(public, http.FileServer(http.Dir(root)))

	if public != "/" && public[len(public)-1] != '/' {
		r.Get(public, http.RedirectHandler(public+"/", 301).ServeHTTP)
		public += "/"
	}

	r.Get(public+"*", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		file := strings.Replace(r.RequestURI, public, "/", 1)
		if _, err := os.Stat(root + file); os.IsNotExist(err) {
			http.ServeFile(w, r, path.Join(root, "index.html"))
			return
		}
		fs.ServeHTTP(w, r)
	}))
}

func init() {
	// ruoter
	router = chi.NewRouter()
	// logs the panic and returns a HTTP 500 (Internal Server Error)
	// status if possible.
	router.Use(middleware.Recoverer)
}

func logger() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Log the request
		fmt.Println(time.Now(), r.Method, r.URL)
		// dispatch the request
		router.ServeHTTP(w, r)
	})
}

func main() {
	// router configuration
	routers()
	// db conection initialization
	db.InitDB()
	// log that indicates that the endpoints has been served successfully
	fmt.Println("Services running...")
	// listen and serve in the port, log the request
	http.ListenAndServe(":8001", logger())

}

package services

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"github.com/PuerkitoBio/goquery"
)

import (
	"whoru/db"
	"whoru/models"
)

// Check : service to check domain
func Check(w http.ResponseWriter, r *http.Request) {

	// get query parameter
	keys, ok := r.URL.Query()["host"]

	// check if query parameter has been send
	if !ok || len(keys[0]) < 1 {
		fmt.Fprint(w, "Url Param 'host' is missing i.e. host?host=truora.com")
		return
	}

	var response models.Servers
	domain := keys[0]

	// create a new scruct
	hostDataResponse := new(models.HostData)

	// request to ssllabs
	makeJSONRequest("https://api.ssllabs.com/api/v3/analyze?host="+domain, hostDataResponse)

	document, err := getDocument("https://" + domain)

	// get from db the report of 1 hour ago
	latestHourReport := db.GetLatestServersReport(domain)

	// if the request to the domain has no errors, try to get the dom elements
	if err == nil {
		// extract the icon from dom
		iconLocation := getIcon(domain, document)

		// extract title from dom
		titleContent := getTitle(document)

		// build response
		response = models.Servers{
			Servers:          []models.Server{},
			ServersChanged:   true,
			SslGrade:         "",
			PreviousSslGrade: latestHourReport.SslGrade,
			Logo:             iconLocation,
			Title:            titleContent,
			IsDown:           false,
		}

		// foreach endpoint check the whois information
		for _, endpointData := range hostDataResponse.Endpoints {
			ipDataResponse := new(models.IpData)
			makeJSONRequest("http://free.ipwhois.io/json/"+endpointData.IPAddress, ipDataResponse)
			response.Servers = append(response.Servers, models.Server{
				Adress:   endpointData.IPAddress,
				SslGrade: endpointData.Grade,
				Country:  ipDataResponse.CountryCode,
				Owner:    ipDataResponse.Isp,
			})

			// update the value of the wors grade: A+ < A < A- < B < C < D < E < F
			if endpointData.Grade > response.SslGrade {
				response.SslGrade = endpointData.Grade
			}
		}

	} else {
		// if the request fail, means that the domain in down
		response.IsDown = true

		// put the previous ssl grade
		response.PreviousSslGrade = latestHourReport.SslGrade
	}

	// check if the response has changed from the latest report
	response.ServersChanged = checkServersHasChange(response.Servers, latestHourReport.Servers)

	// register the response in the db
	db.RegisterServerInfo(domain, response)

	// return the encoded response
	json.NewEncoder(w).Encode(response)
}

// History : service to return all check history
func History(w http.ResponseWriter, r *http.Request) {

	// get the query string limit
	keys, ok := r.URL.Query()["limit"]

	// the default limit for history request is 30
	limit := "30"

	// if the request has no limit, assign the default one
	if ok && len(keys[0]) > 0 {
		limit = keys[0]
	}

	// get the history from db
	response := db.GetCheckHistory(limit)

	// return the encoded response
	json.NewEncoder(w).Encode(response)
}

func checkServersHasChange(a, b []models.Server) bool {

	if (a == nil) == (b == nil) {
		return true
	}

	if (a == nil) != (b == nil) {
		return true
	}

	if len(a) != len(b) {
		return true
	}

	counter := 0
	// check if the domain information has changed
	for i := range a {
		for j := range b {
			if a[i].Adress == b[j].Adress &&
				a[i].SslGrade == b[j].SslGrade &&
				a[i].Country == b[j].Country &&
				a[i].Owner == b[j].Owner {
				counter++
			}
		}
	}

	if counter != len(a) {
		return true
	}

	return false
}

func makeJSONRequest(url string, target interface{}) {
	// make the request
	resp, err := http.Get(url)
	if err != nil {
		fmt.Println(err)
	}

	// decode the response with the provided struct
	error := decode(resp.Body, target)

	if error != nil {
		fmt.Println(error)
	}

	return
}

func getDocument(domain string) (doc *goquery.Document, err error) {
	// verify is domain is down
	_, e := http.Get(domain)
	if e != nil {
		return nil, e
	}
	// parse the request to the response to a Dom object
	doc, err = goquery.NewDocument(domain)
	return
}

func getIcon(domain string, doc *goquery.Document) string {
	// use a selector to query the icon in the dom
	iconValue, iconExist := doc.Find("link[rel='shortcut icon']").Attr("href")

	if !iconExist {
		iconValue, iconExist = doc.Find("link[rel='icon']").Attr("href")
	}

	// if the page does not have icon, use the google service to provide the
	// favorite icon
	if !iconExist {
		iconValue = "https://www.google.com/s2/favicons?domain=" + domain
	}

	return iconValue
}

func getTitle(doc *goquery.Document) string {
	// query the title element from the dom
	return doc.Find("title").Text()
}

func decode(r io.Reader, target interface{}) (err error) {
	if err = json.NewDecoder(r).Decode(target); err != nil {
		return
	}
	return
}

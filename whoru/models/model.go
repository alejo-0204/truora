package models

// DateFormat : date format
var DateFormat = "20060102150405"

// History : history model
type History struct {
	Items []ExtendedServers `json:"items"`
}

// Server : server model
type Server struct {
	Adress   string `json:"adress"`
	SslGrade string `json:"ssl_grade"`
	Country  string `json:"country"`
	Owner    string `json:"owner"`
}

// Servers : servers model
type Servers struct {
	Servers          []Server
	ServersChanged   bool   `json:"servers_changed"`
	SslGrade         string `json:"ssl_grade"`
	PreviousSslGrade string `json:"previous_ssl_grade"`
	Logo             string `json:"logo"`
	Title            string `json:"title"`
	IsDown           bool   `json:"is_down"`
}

// ExtendedServers : extended servers model
type ExtendedServers struct {
	Servers
	Domain string
	Date   string
}

// Endpoints : endpoint model
type Endpoints struct {
	IPAddress         string `json:"ipAddress"`
	ServerName        string `json:"serverName"`
	StatusMessage     string `json:"statusMessage"`
	Grade             string `json:"grade"`
	GradeTrustIgnored string `json:"gradeTrustIgnored"`
	HasWarnings       bool   `json:"hasWarnings"`
	IsExceptional     bool   `json:"isExceptional"`
	Progress          int64  `json:"progress"`
	Duration          int64  `json:"duration"`
	Delegation        int64  `json:"delegation"`
}

// HostData : hostData model
type HostData struct {
	Host            string `json:"host"`
	Port            int64  `json:"Port"`
	Protocol        string `json:"protocol"`
	IsPublic        bool   `json:"isPublic"`
	Status          string `json:"status"`
	StartTime       int64  `json:"startTime"`
	TestTime        int64  `json:"testTime"`
	EngineVersion   string `json:"engineVersion"`
	CriteriaVersion string `json:"criteriaVersion"`
	Endpoints       []Endpoints
}

// IpData : ipData model
type IpData struct {
	IP                string `json:"ip"`
	Success           bool   `json:"success"`
	TypeIP            string `json:"type_ip"`
	Continent         string `json:"continent"`
	ContinentCode     string `json:"continent_code"`
	Country           string `json:"country"`
	CountryCode       string `json:"country_code"`
	CountryFlag       string `json:"country_flag"`
	CountryCapital    string `json:"country_capital"`
	CountryPhone      string `json:"country_phone"`
	CountryNeighbours string `json:"country_neighbours"`
	Region            string `json:"region"`
	City              string `json:"city"`
	Latitude          string `json:"latitude"`
	Longitude         string `json:"longitude"`
	Asn               string `json:"asn"`
	Org               string `json:"org"`
	Isp               string `json:"isp"`
	Timezone          string `json:"timezone"`
	TimezoneName      string `json:"timezone_name"`
	TimezoneDstOffset string `json:"timezone_dstOffset"`
	TimezoneGmtOffset string `json:"timezone_gmtOffset"`
	TimezoneGmt       string `json:"timezone_gmt"`
	Currency          string `json:"currency"`
	CurrencyCode      string `json:"currency_code"`
	CurrencySymbol    string `json:"currency_symbol"`
	CurrencyRates     string `json:"currency_rates"`
	CurrencyPlural    string `json:"currency_plural"`
}

// DomData : dom string model
type DomData string

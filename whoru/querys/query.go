package querys

import (
	"fmt"
	"strconv"
)

// CreateServerTable : sql to create server table
var CreateServerTable = `CREATE TABLE IF NOT EXISTS server (
	id INT PRIMARY KEY, 
	domain STRING,
	serversChanged BOOL,
	sslGrade STRING,
	previousSslGrade STRING,
	logo STRING,
	title STRING,
	isDown BOOL )`

// CreateAdressTable : sql to create adress table
var CreateAdressTable = `CREATE TABLE IF NOT EXISTS adress (
	adress STRING, 
	sslGrade STRING,
	country STRING,
	owner STRING,
	parentId INT NOT NULL REFERENCES server (id) ON DELETE CASCADE,
	INDEX (parentId) )`

// is better to use database/sql package to construct SQL

// GetAllOrderedServer : sql to get all ordered servers check
func GetAllOrderedServer(limit string) string {
	return fmt.Sprintf("SELECT * FROM server order by id desc LIMIT %s", limit)
}

// InsServer : sql to insert server
func InsServer(parentID string, domain string, serversChanged bool, sslGrade string, previousSslGrade string, logo string, title string, isDown bool) string {
	return fmt.Sprintf("INSERT INTO server VALUES (%s,'%s','%s','%s','%s','%s','%s',%s)",
		parentID,
		domain,
		strconv.FormatBool(serversChanged),
		sslGrade,
		previousSslGrade,
		logo,
		title,
		strconv.FormatBool(isDown))
}

// InsAdress : sql to insert adress
func InsAdress(adress string, sslGrade string, country string, owner string, parentID string) string {
	return fmt.Sprintf("INSERT INTO adress VALUES ('%s','%s','%s','%s',%s)",
		adress,
		sslGrade,
		country,
		owner,
		parentID)
}

// GetAdressByParentID : sql to get all adress from a parentID
func GetAdressByParentID(parentID int64) string {
	return fmt.Sprintf("SELECT * FROM adress where parentId = %d", parentID)
}

// GetLastHourReport : sql to get last hour report
func GetLastHourReport(id string, domain string) string {
	// domain like
	return fmt.Sprintf("SELECT * FROM server where id <= %s and domain = '%s' LIMIT  1", id, domain)
}

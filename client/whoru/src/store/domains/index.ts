import { IFullState } from "@/domain/models";
import { Module } from "vuex";
import { IRootState } from "../../domain/models";
import { actions } from "./actions";
import { getters } from "./getters";
import { mutations } from "./mutations";

export const state: IFullState = {
  history: [],
  error: undefined,
  searchResult: undefined,
  loading: false
};

export function createStoreConfig(): Module<IFullState, IRootState> {
  return {
    state,
    getters,
    actions,
    mutations
  };
}

import Vue from "vue";
import Vuex, { StoreOptions } from "vuex";
import { IRootState } from "../domain/models";
import { createStoreConfig } from "./domains/index";

Vue.use(Vuex);

const store: StoreOptions<IRootState> = {
  modules: { domains: createStoreConfig() }
};

export default new Vuex.Store<IRootState>(store);

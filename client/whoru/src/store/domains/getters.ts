import { IDomain, IFullState } from "@/domain/models";
import { GetterTree } from "vuex";
import { IRootState } from "../../domain/models";

export const getters: GetterTree<IFullState, IRootState> = {
  allDomains(state): IDomain[] {
    return state.history;
  },
  searchResult(state): IDomain | undefined {
    return state.searchResult;
  },
  isLoading(state): boolean {
    return state.loading;
  }
};

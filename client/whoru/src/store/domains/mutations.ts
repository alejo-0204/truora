import { IDomain, IFullState } from "@/domain/models";
import { MutationTree } from "vuex";

export const mutations: MutationTree<IFullState> = {
  historyLoaded(state, payload: IDomain[]) {
    state.history = payload;
    state.loading = false;
  },
  searchResult(state, payload: IDomain) {
    if (state.history) {
      state.history = [payload, ...state.history];
    } else {
      state.history = [payload];
    }
    state.searchResult = payload;
    state.loading = false;
  },
  loading(state) {
    state.loading = true;
  },
  error(state, payload: string) {
    state.error = { message: payload };
  }
};

import axios from "axios";

export async function getDomainInfo(domain: string) {
  return await axios
    .get(`/api/v1/check?host=${domain}`)
    .then(res => res.data)
    .catch(err => console.info(err));
}

export async function getHistory() {
  return await axios
    .get(`/api/v1/history?limit=30`)
    .then(res => res.data)
    .catch(err => console.info(err));
}

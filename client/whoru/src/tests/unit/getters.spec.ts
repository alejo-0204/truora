import { getters } from "@/store/domains/getters";
import { IFullState, IDomain } from "@/domain/models";

describe("Getters", () => {
  it("should return the loading flag", () => {
    const state: IFullState = { history: [], loading: true };
    const result = getters.isLoading(state, {}, { version: "1.0" }, {});
    expect(result).toBe(true);
  });

  it("should return the search history", () => {
    const history: IDomain[] = [
      {
        Servers: null,
        servers_changed: true,
        ssl_grade: "",
        previous_ssl_grade: "",
        logo:
          "https://uploads-ssl.webflow.com/5b559a554de48fbcb01fd277/5b97f0ac932c3291fa40d053_icon32.png",
        title: "TruoraTestTest",
        is_down: false,
        Domain: "truora.com",
        Date: "2019-06-07 21:09:36"
      },
      {
        Servers: [
          {
            adress: "172.217.0.46",
            ssl_grade: "A",
            country: "US",
            owner: "Google LLC"
          },
          {
            adress: "2607:f8b0:4005:802:0:0:0:200e",
            ssl_grade: "A",
            country: "US",
            owner: "Google LLC"
          }
        ],
        servers_changed: true,
        ssl_grade: "A",
        previous_ssl_grade: "",
        logo: "https://www.google.com/s2/favicons?domain=google.com",
        title: "Google",
        is_down: false,
        Domain: "google.com",
        Date: "2019-06-07 21:09:32"
      },
      {
        Servers: null,
        servers_changed: true,
        ssl_grade: "",
        previous_ssl_grade: "",
        logo: "https://s.ytimg.com/yts/img/favicon-vfl8qSV2F.ico",
        title: "YouTube",
        is_down: false,
        Domain: "youtube.com",
        Date: "2019-06-07 21:09:28"
      }
    ];

    const state: IFullState = { history, loading: true };
    const result = getters.allDomains(state, {}, { version: "1.0" }, {});
    expect(result).toEqual(history);
  });

  it("should return the search result", () => {
    const searchResult: IDomain = {
      Servers: [
        {
          adress: "172.217.0.46",
          ssl_grade: "A",
          country: "US",
          owner: "Google LLC"
        },
        {
          adress: "2607:f8b0:4005:802:0:0:0:200e",
          ssl_grade: "A",
          country: "US",
          owner: "Google LLC"
        }
      ],
      servers_changed: true,
      ssl_grade: "A",
      previous_ssl_grade: "",
      logo: "https://www.google.com/s2/favicons?domain=google.com",
      title: "Google",
      is_down: false,
      Domain: "google.com",
      Date: "2019-06-07 21:09:32"
    };

    const state: IFullState = { history: [], loading: false, searchResult };
    const result = getters.searchResult(state, {}, { version: "1.0" }, {});
    expect(result).toEqual(searchResult);
  });
});
